package pojos;
// Generated 31 ene 2022 14:58:49 by Hibernate Tools 5.4.32.Final

/**
 * ActorInfo generated by hbm2java
 */
public class ActorInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private ActorInfoId id;

	public ActorInfo() {
	}

	public ActorInfo(ActorInfoId id) {
		this.id = id;
	}

	public ActorInfoId getId() {
		return this.id;
	}

	public void setId(ActorInfoId id) {
		this.id = id;
	}

}
