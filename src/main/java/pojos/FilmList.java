package pojos;
// Generated 31 ene 2022 14:58:49 by Hibernate Tools 5.4.32.Final

/**
 * FilmList generated by hbm2java
 */
public class FilmList implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private FilmListId id;

	public FilmList() {
	}

	public FilmList(FilmListId id) {
		this.id = id;
	}

	public FilmListId getId() {
		return this.id;
	}

	public void setId(FilmListId id) {
		this.id = id;
	}

}
