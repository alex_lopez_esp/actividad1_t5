package app;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;

import antlr.collections.List;
import pojos.Actor;
import pojos.Category;
import pojos.Film;
import pojos.FilmActor;
import pojos.FilmActorId;
import pojos.FilmCategory;
import pojos.FilmCategoryId;
import pojos.Language;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unused")
		org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);
		
		System.out.println("APP - GESTI�N TAREAS");
		
		Session s=Conexion.getSession();
		System.out.println("Hola amigo");
		parte1(s);
		
		//parte3(s);
		//parte4(s);
//		Category c=s.get(Category.class,(byte) 1);
//		ArrayList<String>listadoP=findFilmByCat(c,s);
//		
//		listadoP.forEach(System.out::println);
		
	
	
	}
	public static void parte1(Session s) {
		
		
		
		
		Category category1=s.get(Category.class, (byte)11);
		Category category2=s.get(Category.class, (byte)10);
		
		
		
		Language langue=s.get(Language.class,(byte) 1);
		Date date=new Date();
		
		Film f=new Film(langue,"Alex y sus guerras",(byte)90,new BigDecimal(20),new BigDecimal(100),date);
		s.beginTransaction();
		s.save(f);
		s.getTransaction().commit();
		System.out.println("�Insertado pelicula!");

		
		
		FilmCategoryId filmCategory1=new FilmCategoryId(f.getFilmId(),category1.getCategoryId());
		FilmCategoryId filmCategory2=new FilmCategoryId(f.getFilmId(),category2.getCategoryId());
		FilmCategory filmCategorygame=new FilmCategory(filmCategory1,category1,f,date);
		FilmCategory filmCategoryhorror=new FilmCategory(filmCategory2,category2,f,date);
		s.beginTransaction();
		s.save(filmCategorygame);
		s.save(filmCategoryhorror);
		s.getTransaction().commit();
		System.out.println("�Insertado datosPelicula!");
		parte2(s,f);
		
	
	}
	
	public static void parte2(Session s,Film f) {
			Actor yo = new Actor("Alex", "Lopez Espa�a", new Date(), null);
			s.save(yo);
			
			Film film = s.get(Film.class, f.getFilmId());
			Actor actor = s.get(Actor.class,(short) 1);
			FilmActorId yoActorId=new FilmActorId(yo.getActorId(),film.getFilmId());
			FilmActorId actorRandomId=new FilmActorId(actor.getActorId(),film.getFilmId());
			
			
			FilmActor filmActoryo=new FilmActor(yoActorId,yo,film,new Date());
			FilmActor filmActorRandom=new FilmActor(actorRandomId,actor,film,new Date());
			
			s.beginTransaction();
			s.save(filmActoryo);
			s.save(filmActorRandom);
			s.getTransaction().commit();
			
			

			

		}
	
	
		
	public static void parte3(Session s) {
		Film f=s.get(Film.class,(short)1);
		
		
		ArrayList<Object[]> listadoP = (ArrayList<Object[]>)s.createQuery("select f.title,f.releaseYear from Film f join f.filmCategories fc where fc.category.name like 'Action'", Object[].class).list();
		listadoP.forEach((reg) -> {
			System.out.println("Titulo Pelicula: " + reg[0] + " -  A�o de lanzamiento: " + reg[1]);
		});

		
		
	}
	public static void parte4(Session s) {
		Film f=s.get(Film.class,(short)1);
		
		
		ArrayList<Object[]> listadoP = (ArrayList<Object[]>)s.createQuery("select f.title,count(fa.actor.actorId) from Film f join f.filmCategories fc join f.filmActors fa  where fc.category.name like 'Horror' group by f.title", Object[].class).list();
		listadoP.forEach((reg) -> {
			System.out.println("Titulo Pelicula: " + reg[0] + " -  Numero de actores: " + reg[1]);
		});
		
	}
	public static ArrayList<String> findFilmByCat(Category c,Session s) {
	
		ArrayList<String> listadoP = (ArrayList<String>) s.createQuery("select f.title from Film f join f.filmCategories fc where fc.category like "+c.getCategoryId(), String.class).list();
		
		return listadoP;
	}


}
